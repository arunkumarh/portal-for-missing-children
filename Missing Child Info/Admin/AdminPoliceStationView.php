   
  <?php
include_once "../DB/db.php";

 $ID=$_GET['ID'];
 
 $sql = "SELECT * FROM tblpolicestation where ID= $ID";
 $result=execute($sql);	
 if($row = $result->fetch_assoc())
 {
 	$ID=$row['ID'];
	$PoliceStation=$row['PoliceStation'];
	$Code=$row['Code'];
	$PSIName=$row['PSIName'];
	$LandLine=$row['LandLine'];
	$Mobile=$row['Mobile'];
	$Address=$row['Address'];
	$District=$row['District'];
 }

 if (isset($_POST['btndelete']))
 {
	$sql="DELETE FROM tblpolicestation where ID=$ID";
	$result=execute($sql);	
			 
	$sql="DELETE FROM tbllogin where UserID='$Code'";
	$result=execute($sql);	
	if($result)
	{
		echo "<script type='text/javascript'> alert('Deleted Successfully');</script>";
		echo "<meta http-equiv='refresh' content='0;url=AdminPoliceStationList.php'>";
	}
	else
	{
		echo "<script type='text/javascript'> alert('Action not processed');</script>";
	}
}
		
if (isset($_POST['btnupdate']))
{
	$PoliceStation=$_POST['txtPoliceStation'];
	$Code=$_POST['txtCode'];
	$PSIName=$_POST['txtPSIName'];
	$LandLine=$_POST['txtLandLine'];
	$Mobile=$_POST['txtMobile'];
	$Address=$_POST['txtAddress'];
	
	$sql="Update tblpolicestation SET PoliceStation='$PoliceStation',Code='$Code',PSIName='$PSIName',LandLine='$LandLine',Mobile='$Mobile',Address='$Address' where ID=$ID";
	$result=execute($sql);	
	if($result)
	{
		echo "<script type='text/javascript'> alert('Updated Successfully');</script>";
		echo "<meta http-equiv='refresh' content='0;url=AdminPoliceStationList.php'>";
	}
	else
	{
		echo "<script type='text/javascript'> alert('Action not processed');</script>";
	}
}
?>

<?php
  include("../Masterpages/AdminHeader.php");
?>
  
   <h3> Police Station Details</h3>
 
 <form id="frmaddpolicestation" name="frmaddpolicestation" method="post" action="">
           	<table id="minitable">
                 <tr>
                	<td>ID </td>
                    <td><label id="l1"><?php echo $ID; ?></label> <input type="text" name="txtID" readonly="readonly" class="hide" value="<?php echo $ID; ?>"/></td>
                </tr>
                
                 <tr>
                	<td> Police Station </td>
					<td><label id="l2"><?php echo $PoliceStation; ?></label> 
                    <input type="text" name="txtPoliceStation" maxlength="100" class="hide" value="<?php echo $PoliceStation; ?>" /></td>
                </tr>
                
                 <tr>
                	<td> Code </td>
                    <td><label id="l3"><?php echo $Code; ?></label> 
                    <input type="text" name="txtCode" maxlength="30" class="hide" value="<?php echo $Code; ?>" /></td>
                </tr>
                
                   <tr>
                	<td> PSI Name </td>
                     <td><label id="l4"><?php echo $PSIName; ?></label> 
                    <input type="text" name="txtPSIName" maxlength="100" class="hide" value="<?php echo $PSIName; ?>" /></td>
                </tr>
                
                <tr>
                	<td>Land Line</td>
                    <td><label id="l5"><?php echo $LandLine; ?></label> 
                    <input type="text" name="txtLandLine" maxlength="14" class="hide" value="<?php echo $LandLine; ?>" /></td>
                </tr>
                
                 <tr>
                	<td>Mobile</td>
					<td><label id="l6"><?php echo $Mobile; ?></label> 
                    <input type="text" name="txtMobile" maxlength="10" class="hide" onKeyPress="return numbersonly(event, false)" value="<?php echo $Mobile; ?>"/></td>
                </tr>
                
                
                  <tr>
            	<td>Address</td>
                <td><label id="l7"><?php echo $Address; ?></label> 
                <textarea type="text" name="txtAddress" style="height:100px" class="hide"><?php echo $Address; ?></textarea></td>
           		 </tr>
                
				  <tr>
            	<td>District</td>
                <td><?php echo $District; ?></td>
           		 </tr>
				 
                
             <tr>
                	  <td>
                      
                <Input type="submit" name="btndelete" value="Delete" onclick="return confirmSubmit()" id="button"/>
                 <Input type="submit" class="hide" name="btnupdate" value="Update" onclick="return check(frmaddpolicestation)" id="button"/></td>
                 <td>
               <button type="button" name="btnedit" onclick="addInput(this.form);" id="button">Edit</button>
              
               <button type="button" class="hide" name="btncancel" onclick="reloadPage()" id="button" >Cancel</button>
                </td>
                </tr>
                
           </table>
           </form>
  
   <?php
  include("../Masterpages/Footer.php");
  ?>
  
  
  <script language="javascript">
function check(f)
{
 	if(f.txtPoliceStation.value=="")
	{
		alert("Enter Police Station");
        f.txtPoliceStation.focus();
		return false ;
	}
	else if(f.txtCode.value=="")
	{
		alert("Enter Police Station Code");
        f.txtCode.focus();
		return false ;
	}
	else if(f.txtPSIName.value=="")
	{
		alert("Enter PSI Name");
        f.txtPSIName.focus();
		return false ;
	}
	else if(f.txtLandLine.value=="" || checkphone(f.txtLandLine)==false)
	{
		alert("Enter Land Line");
        f.txtLandLine.focus();
		return false ;
	}
	else if (f.txtMobile.value=="" || checkmobile(f.txtMobile)==false)
   	{
        alert("Enter Mobile");
        f.txtMobile.focus();
		return false ;
    }
	else if (f.txtAddress.value=="")
	{
   		alert("Enter Address");
   		f.txtAddress.focus();
   		return false;
	}
	else
		return true;

}
</script>

<style type="text/css">
input {display:block;}
.hide {display:none;} 

textarea {display:block;}
</style>