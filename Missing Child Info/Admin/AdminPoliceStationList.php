  
     
  <?php
  include_once "../DB/db.php";
  include("../MasterPages/AdminHeader.php");
  ?>
  
  <h3>Police Station List</h3>
        
         <button type="button" name="btnadd" onClick="window.location.href='AdminPoliceStationAdd.php'" style="width:200px;">Add Police Station</button>
   
  <br>      
  
   <form id="frmlist" name="frmlist" method="post" action="" enctype="multipart/form-data">
        	
            <table id="logintable">
            <tr>
            <td>District</td>
            <td>
    			 <select name="cmbDistrict"  class="select" onChange="check1()">
        		    <option value="0">Select</option>
      				<?php
					   	include_once "../DB/db.php";
						 $sql = "SELECT DISTINCT(District) FROM tbldistrict";
						$res=execute($sql);	
						
					   	while($result = $res->fetch_assoc())
        				{
	  			      ?>
				            <option <?php echo (isset($_REQUEST['cmbDistrict']) ? (($_REQUEST['cmbDistrict']== $result['District']) ? "Selected" : "") : "")  ?> value = "<?php echo $result['District']?>"><?php echo $result['District'] ?></option>                           
        				<?php
							}
						?>
                        </select>
                        </td>
            </tr>
            </table>
            </form>
			
<?php

if(isset($_REQUEST['cmbDistrict']))
	{
		if($_REQUEST['cmbDistrict']!='0')
		{
			$District=$_REQUEST['cmbDistrict'];
			$sql = "SELECT * FROM tblpolicestation where District='$District'";
			
			$result=execute($sql);	
			if ($result->num_rows > 0) 
			{

?>

	 <table id="fulltable">
     
     <tr><th>Police Station</th>
	 <th>Code</th>
     <th>PSIName</th>
     <th>Mobile</th>
      <th>View</th>
     </tr>
     
     <?php
while($row = $result->fetch_assoc()) 
  { ?>
     <tr>
      <td> <?php echo $row['PoliceStation']; ?></td>
	 <td> <?php echo $row['Code']; ?></td>
      <td> <?php echo $row['PSIName']; ?></td>
       <td> <?php echo $row['Mobile']; ?></td>
   <td><a class="btn" href="AdminPoliceStationView.php?ID=<?php echo $row['ID']; ?>">View</a></td>
	</tr>
<?php
  }
?>
   </table>
   
    <?php
	}
	else
	{
	   echo "No Records Found";
	}
 
 }
}
  ?>
  
     <?php
  include("../MasterPages/Footer.php");
  ?>
  
  <script type="text/javascript">
function check1() {
     document.frmlist.submit()
}
</script>
  
  