<?php

echo '

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Missing Child Info</title>
<meta name="description" content="website description">
<meta name="keywords" content="website keywords, website keywords">
<meta http-equiv="content-type" content="text/html; charset=windows-1252">
	<link rel="stylesheet" type="text/css" href="CSS/style.css" title="style">
	<link rel="stylesheet" type="text/css" href="CSS/mycss.css" title="style">
 	<link href="CSS/js-image-slider.css" rel="stylesheet" type="text/css" />
    <script src="JS/js-image-slider.js" type="text/javascript"></script>
	<script type="text/javascript" src="JS/validation.js"></script>
</head>

<body>
<div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <h1><a href="Index.php">Missing Child <span class="logo_colour">Info Portal</span></a></h1>
          <h2>Find Your Missing Child!</h2>
        </div>
      </div>
	   <div id="menubar">
        <ul id="menu">
             					<li><a href="Index.php">Home</a></li>
			                    <li><a href="MissingKidsList.php">Missing Kids</a></li>
                                <li><a href="FoundKidList.php">Found Kids</a></li>
			                    <li><a href="ContactUs.php">Meet Us</a></li>
			                    <li><a href="Login.php">Login</a></li>
        </ul></div>
    </div>
    <div id="site_content">
	 <div id="sliderFrame">
                <div id="slider">
                   <img src="images/Slide Images/1.png" alt="Missing Child Info Portal" />
                   <img src="images/Slide Images/2.png" alt="" />
                   <img src="images/Slide Images/3.png" alt="" />
                </div>
            </div>'


?>