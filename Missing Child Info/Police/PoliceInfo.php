<?php
session_start();
$Code=$_SESSION['UserID'];

include_once "../DB/db.php";
?>

<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

	$sql = "SELECT * FROM tblpolicestation where Code='$Code'";
	
	$result=execute($sql);	
	
	if($row = $result->fetch_assoc())
 	{
 		$ID=$row['ID'];
		$PoliceStation=$row['PoliceStation'];
		$Code=$row['Code'];
		$PSIName=$row['PSIName'];
		$LandLine=$row['LandLine'];
		$Mobile=$row['Mobile'];
		$Address=$row['Address'];
		$District=$row['District'];
	}
?>


<?php


if (isset($_POST['btnupdate']))
{
	$PSIName=$_POST['txtPSIName'];
	$LandLine=$_POST['txtLandLine'];
	$Mobile=$_POST['txtMobile'];
	$Address=$_POST['txtAddress'];
	
			
	 $sql="UPDATE `tblpolicestation` SET `PSIName`='$PSIName',`LandLine`='$LandLine',`Mobile`='$Mobile',`Address`='$Address' WHERE Code='$Code'";
	    	
	$res=execute($sql);	
		
	if($res)
	{
	
		echo "<script type='text/javascript'> alert('Updated Successfully');</script>";
		echo "<meta http-equiv='refresh' content='0;url=PoliceInfo.php'>";
	}
	else
	{
		echo "<script type='text/javascript'> alert('Action not processed');</script>";
	}
}
	
?>



  <?php
  include("../MasterPages/PoliceHeader.php");
  ?>
  
  <h3>Police Details Page</h3>
  
            
<form id="frminfo" name="frminfo" method="post" action="" enctype="multipart/form-data">
           	<table id="minitable">
            	<tr>
                	<td style="width:50%;">ID </td>
                    <td><?php echo $ID; ?></td>
                </tr>
       			
					
				<tr>
                	<td>Police Station</td>
                    <td><?php echo $PoliceStation; ?></td>
                </tr>
				
				<tr>
                	<td>Code</td>
                    <td><?php echo $Code; ?></td>
                </tr>
				
				<tr>
                	<td>PSI Name</td>
                    <td><label id="l2"><?php echo $PSIName; ?></label>
					 <input type="text" name="txtPSIName" maxlength="100" class="hide" value="<?php echo $PSIName; ?>"/>
					</td>
                </tr>
				
				<tr>
                	<td>Land Line</td>
                    <td><label id="l3"><?php echo $LandLine; ?></label>
					 <input type="text" name="txtLandLine" maxlength="12" class="hide" value="<?php echo $LandLine; ?>"/>
					</td>
                </tr>
				
				<tr>
                	<td>Mobile</td>
					<td><label id="l8"><?php echo $Mobile; ?></label>
					<input type="text" name="txtMobile" maxlength="10" class="hide" value="<?php echo $Mobile; ?>" onKeyPress="return numbersonly(event, false)"/>
					</td>
                </tr>
				
                	
				<tr>
                	<td>Address</td>
					<td> <label id="l2"><?php echo $Address; ?></label> 
                    <textarea type="text" name="txtAddress" style="height:100px;" class="hide"><?php echo $Address; ?></textarea></td>
                </tr>
				
				 <tr>
                	<td>District</td>
					<td><?php echo $District; ?>	</td>
                </tr>
				
				
				
				 				
			
				
               <tr>
                	  <td>
                      
                 <Input type="submit" class="hide" name="btnupdate" value="Update" onclick="return check(frminfo)" id="button"/></td>
                 <td>
               <button type="button" name="btnedit" onclick="addInput(this.form);" id="button">Edit</button>
              
               <button type="button" class="hide" name="btncancel" onclick="reloadPage()" id="button" >Cancel</button>
                </td>
                </tr>
                   
			
			
           </table>
           </form>
         
  
  
    <?php
  include("../MasterPages/Footer.php");
  ?>
  
  <style type="text/css">
input {display:block;}
.hide {display:none;} 

textarea {display:block;}
</style>



  <script language="javascript">
function check(f)
{
 	if(f.txtPSIName.value=="")
	{
		alert("Enter PSI Name");
        f.txtPSIName.focus();
		return false ;
	}
	else if(f.txtLandLine.value=="" || checkphone(f.txtLandLine)==false)
	{
		alert("Enter Land Line");
        f.txtLandLine.focus();
		return false ;
	}
	else if (f.txtMobile.value=="" || checkmobile(f.txtMobile)==false)
   	{
        alert("Enter Mobile");
        f.txtMobile.focus();
		return false ;
    }
	else if (f.txtAddress.value=="")
	{
   		alert("Enter Address");
   		f.txtAddress.focus();
   		return false;
	}
	else
		return true;

}
</script>
